# Generated by Django 3.0.2 on 2021-03-11 07:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('usr', '0003_auto_20210311_0632'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='id',
        ),
        migrations.AddField(
            model_name='product',
            name='cat_name',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='categ_name', to='usr.Category'),
        ),
        migrations.AddField(
            model_name='product',
            name='cate_id',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='categ_id', to='usr.Category'),
        ),
        migrations.AddField(
            model_name='product',
            name='shop_id',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='img', serialize=False, to='usr.Shop'),
            preserve_default=False,
        ),
    ]
