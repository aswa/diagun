# Generated by Django 3.0.2 on 2021-03-11 09:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usr', '0009_auto_20210311_0902'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shop',
            name='shop_id',
        ),
    ]
