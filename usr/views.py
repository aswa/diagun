from django.shortcuts import get_object_or_404
from .serializer import ShopSerializer

from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Shop
from rest_framework import viewsets, status






class ShopViewSet(viewsets.ModelViewSet):
    serializer_class = ShopSerializer
    queryset = Shop.objects.all()


    def retrieve(self,request,pk):

        instance = Shop.objects.get(pk=pk)
        serializer = ShopSerializer(instance)
        return Response(serializer.data)







