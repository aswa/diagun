from rest_framework import serializers
from .models import Shop,Category,Media,Product




class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['id','product_name','products_image']

class CategorySerializer(serializers.ModelSerializer):


    class Meta:
        model = Category
        fields = ['id','category_name']

class ShopSerializer(serializers.ModelSerializer):
    data = CategorySerializer(many=True, read_only=True)
    product = ProductSerializer(many=True, read_only=True)


    class Meta:
         model = Shop
         fields = ["id","data","product"]

