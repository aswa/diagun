from django.contrib import admin
from .models import Category,Shop,Product,Media
# Register your models here.
admin.site.register(Category)
admin.site.register(Shop)
admin.site.register(Product)
admin.site.register(Media)
