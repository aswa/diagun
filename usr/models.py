from django.db import models

# Create your models here.



class Shop(models.Model):


    shop_name = models.CharField(max_length=100)
    shop_location = models.CharField(max_length=100)


    def __str__(self):
        return self.shop_name

class Category(models.Model):
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE, related_name="data")
    category_name = models.CharField(max_length=100)
    parant_cat = models.CharField(max_length=100)

    def __str__(self):
        return self.category_name

class Media(models.Model):

    image_name = models.CharField(max_length=100,null=True)
    product_image = models.ImageField(upload_to='pics')

    def __str__(self):
        return self.image_name



class Product(models.Model):

    shops = models.ForeignKey(Shop, on_delete=models.CASCADE, related_name="product")
    Category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="cat")
    product_name = models.CharField(max_length=100,null=True)
    products_image = models.ForeignKey(Media,on_delete=models.CASCADE,related_name="img")

    
    def __str__(self):
        return self.product_name



